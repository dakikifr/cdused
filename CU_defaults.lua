--[[
  Cooldown Used by Kiki/Esp�rance - European Conseil des Ombres
]]


--------------- Shared Constantes ---------------

CU_SPELLS_COOLDOWN_USED = 1; -- Spell used by raid member on someone
CU_SPELLS_AURA_GAINED = 2; -- Raid member gained an aura
CU_SPELLS_AURA_LOST = 3; -- Raid member lost an aura
CU_SPELLS_INTERRUPTED = 4; -- Raid member interrupted someone
CU_SPELLS_INTERRUPT_USED_SUCCESS = 5; -- SpellCastSuccess with an interrupt spell
CU_SPELLS_INTERRUPT_USED_FAILED = 6; -- SpellFailed with an interrupt
CU_SPELLS_TAUNTED = 7; -- Raid member successfully taunted
CU_SPELLS_TAUNT_MISSED = 8; -- Raid member failed to taunt
CU_SPELLS_RESURRECT = 9; -- Resurrection spell used
CU_SPELLS_CREATIONS = 10; -- Raid member creating things
CU_SPELLS_FEAST = 11; -- Feasts
CU_SPELL_CAST_SUCCESS = 12; -- Spell Cast success

CU_SpellTypeToName = {
 [CU_SPELLS_COOLDOWN_USED] = "CooldownUsed",
 [CU_SPELLS_AURA_GAINED] = "AuraGained",
 [CU_SPELLS_AURA_LOST] = "AuraLost",
 [CU_SPELLS_INTERRUPTED] = "Interrupted",
 [CU_SPELLS_INTERRUPT_USED_SUCCESS] = "InterruptUsedSuccess",
 [CU_SPELLS_INTERRUPT_USED_FAILED] = "InterruptUsedFailed",
 [CU_SPELLS_TAUNTED] = "Taunted",
 [CU_SPELLS_TAUNT_MISSED] = "TauntMissed",
 [CU_SPELLS_RESURRECT] = "SpellRez",
 [CU_SPELLS_CREATIONS] = "Creations",
 [CU_SPELLS_FEAST] = "Feast",
};

--------------- Spells ---------------

-- Spell creations
local TOY_TRAIN_SET = 61031;

-- Feasts
local GREAT_FEAST = 57301;
local FISH_FEAST = 57426;
local BOUNTIFUL_FEAST = 66476;
local DRAGON_FEAST = 87643;
local SEAFOOD_FEAST = 87644;
local GOBELIN_BARBECUE_FEAST = 87915; -- 84351

-- Cauldron
local CAULDRON_OF_BATTLE = 92649;

-- Cooldown spells
 -- Death Knight
 local ICEBOUND_FORTITUDE = 48792;
 local VAMPIRIC_BLOOD = 55233;
 local DANCING_RUNE_WEAPON = 81256;
 local ANTIMAGIC_SHELL = 48707;
 local RAISE_ALLY = 61999;
 -- Druid
 local REBIRTH = 20484;
 local SURVIVAL_INSTINCTS = 61336;
 local FRENZIED_REGENERATION = 22842;
 local BARKSKIN = 22812;
 -- Hunter
 local MISDIRECTION = 34477;
 local FEIGN_DEATH = 5384;
 local DETERRENCE = 19263;
 -- Mage
 local ICE_BLOCK = 45438;
 local INVISIBILITY = 66;
 local TIME_WARP = 80353;
 -- Paladin
 local DIVINE_SHIELD = 642;
 local DIVINE_PROTECTION = 498;
 local BLESSING_OF_SACRIFICE = 6940;
 local LAY_ON_HANDS = 633;
 local ARDENT_DEFENDER = 31850;
 local AURA_MASTERY = 31821;
 local BLESSING_OF_PROTECTION = 1022;
 local GUARDIAN_ANCIENT_KINGS = 86659; -- Currently bugged, only the SPELL_CAST is in the combat log, AURA_GAINED/AURA_LOST is not shown
 local SHIELD_OF_VENGEANCE = 184662; -- Retribution
 -- Priest
 local PAIN_SUPPRESSION = 33206;
 local GUARDIAN_SPIRIT = 47788;
 local POWER_INFUSION = 10060;
 local LEAP_OF_FAITH = 73325;
 local POWER_WORD_BARRIER = 62618;
 local SYMBOL_OF_HOPE = 64901;
 local DIVINE_HYMN = 64843;
 -- Rogue
 local TRICKOFTRADE = 57934;
 local CLOAK_OF_SHADOWS = 31224;
 -- Shaman
 local BLOOD_LUST = 2825;
 local HEROISM = 32182;
 local SPIRIT_LINK_TOTEM = 98008;
 local SPIRIT_WALKER_GRACE = 79206;
 local HEALING_TIDE_TOTEM = 108280;
 local ASTRAL_SHIFT = 108271;
 -- Warlock
 local SOUL_SHATTER = 29858;
 -- Warrior
 local LAST_STAND = 12975;
 local SHIELD_WALL = 871;
 -- Monk
 local FORTIFYING_BREW = 115203; -- Currently bugged, only the SPELL_CAST is in the combat log, AURA_GAINED/AURA_LOST is not shown
 local ZEN_MEDITATION = 115176;
 local DIFFUSE_MAGIC = 122783;
 local DAMPEN_HARM = 122278;
 local REVIVAL = 115310;
 local LIFE_COCOON = 116849;

-- Taunts
 -- Death Knight
 local DARK_COMMAND = 56222;
 -- Druid
 local GROWL = 6795;
 -- Hunter
 local DISTRACTING_SHOT = 20736;
 -- Paladin
 local RECKONING = 62124;
 -- Warrior
 local TAUNT = 355;
 -- Monk
 local PROVOKE = 115546;
 -- Demon Hunter
 --local  = ;
 -- Pets
 local GROWL_PET = 2649;

-- Interrupts
 -- Death Knight
 local MIND_FREEZE = 47528;
 -- Demon Hunter
 local CONSUME_MAGIC = 183752;
 -- Druid
 local MAIM_INTERRUPT = 32747; -- Druid's Maim
 local MIGHTY_BASH = 5211;
 local SKULL_BASH_INTERRUPT = 93985;
 local SOLAR_BEAM_INTERRUPT = 97547;
 -- Hunter
 local COUNTER_SHOT = 147362;
 local MUZZLE = 187707;
 -- Mage
 local COUNTERSPELL = 2139;
 -- Monk
 local SPEAR_HAND_STRIKE = 116705;
 -- Paladin
 local AVENGERS_SHIELD = 31935;
 local HAMMER_OF_JUSTICE = 853;
 local REBUKE = 96231;
 -- Priest
 local SILENCE = 15487;
 -- Rogue
 local KICK = 1766;
 -- Shaman
 local WIND_SHEAR = 57994;
 -- Warlock
 local SPELL_LOCK = 132409;
 -- Warrior
 local PUMMEL = 6552;
 -- Common
 local ARCANE_TORRENT_MANA = 28730;
 local ARCANE_TORRENT_ENERGY = 25046;
 local ARCANE_TORRENT_RAGE = 69179;
 local ARCANE_TORRENT_FOCUS = 80483;
 local ARCANE_TORRENT_RUNIC_POWER = 50613;
 local QUAKING_PALM = 107079;

--------------- Templates ---------------

CU_Template_All = {
 [CU_SPELLS_CREATIONS] = {
  [TOY_TRAIN_SET] = true,
 },
 [CU_SPELLS_FEAST] = {
  [GREAT_FEAST] = true,
  [FISH_FEAST] = true,
  [BOUNTIFUL_FEAST] = true,
  [DRAGON_FEAST] = true,
  [SEAFOOD_FEAST] = true,
  [GOBELIN_BARBECUE_FEAST] = true,
  [CAULDRON_OF_BATTLE] = true,
 },
 [CU_SPELLS_AURA_GAINED] = {
  -- Death Knight
  [ICEBOUND_FORTITUDE] = true,
  [VAMPIRIC_BLOOD] = true,
  [DANCING_RUNE_WEAPON] = true,
  [ANTIMAGIC_SHELL] = true,
  --[] = true,
  -- Druid
  [SURVIVAL_INSTINCTS] = true,
  [FRENZIED_REGENERATION] = true,
  [BARKSKIN] = true,
  --[] = true,
  -- Hunter
  [FEIGN_DEATH] = true,
  [DETERRENCE] = true,
  --[] = true,
  -- Mage
  [ICE_BLOCK] = true,
  [INVISIBILITY] = true,
  --[] = true,
  -- Paladin
  [DIVINE_SHIELD] = true,
  [DIVINE_PROTECTION] = true,
  [BLESSING_OF_SACRIFICE] = true,
  [ARDENT_DEFENDER] = true,
  [BLESSING_OF_PROTECTION] = true,
  [GUARDIAN_ANCIENT_KINGS] = true,
  [SHIELD_OF_VENGEANCE] = true,
  --[] = true,
  -- Priest
  [PAIN_SUPPRESSION] = true,
  [GUARDIAN_SPIRIT] = true,
  [POWER_INFUSION] = true,
  --[] = true,
  -- Rogue
  [CLOAK_OF_SHADOWS] = true,
  --[] = true,
  -- Shaman
  [ASTRAL_SHIFT] = true,
  -- Warrior
  [LAST_STAND] = true,
  [SHIELD_WALL] = true,
  -- Monk
  [FORTIFYING_BREW] = true,
  [ZEN_MEDITATION] = true,
  [DIFFUSE_MAGIC] = true,
  [DAMPEN_HARM] = true,
  [LIFE_COCOON] = true,
  
  --[] = true,
 },
 [CU_SPELLS_AURA_LOST] = {
  -- Death Knight
  [ICEBOUND_FORTITUDE] = true,
  [VAMPIRIC_BLOOD] = true,
  [DANCING_RUNE_WEAPON] = true,
  [ANTIMAGIC_SHELL] = true,
  --[] = true,
  -- Druid
  [SURVIVAL_INSTINCTS] = true,
  [FRENZIED_REGENERATION] = true,
  [BARKSKIN] = true,
  --[] = true,
  -- Hunter
  [FEIGN_DEATH] = true,
  [DETERRENCE] = true,
  --[] = true,
  -- Mage
  [ICE_BLOCK] = true,
  [INVISIBILITY] = true,
  --[] = true,
  -- Paladin
  [DIVINE_SHIELD] = true,
  [DIVINE_PROTECTION] = true,
  [BLESSING_OF_SACRIFICE] = true,
  [ARDENT_DEFENDER] = true,
  [BLESSING_OF_PROTECTION] = true,
  [GUARDIAN_ANCIENT_KINGS] = true,
  [SHIELD_OF_VENGEANCE] = true,
  --[] = true,
  -- Priest
  [PAIN_SUPPRESSION] = true,
  [GUARDIAN_SPIRIT] = true,
  [POWER_INFUSION] = true,
  --[] = true,
  -- Rogue
  [CLOAK_OF_SHADOWS] = true,
  --[] = true,
  -- Shaman
  [ASTRAL_SHIFT] = true,
  -- Warrior
  [LAST_STAND] = true,
  [SHIELD_WALL] = true,
  -- Monk
  [FORTIFYING_BREW] = true,
  [ZEN_MEDITATION] = true,
  [DIFFUSE_MAGIC] = true,
  [DAMPEN_HARM] = true,
  [LIFE_COCOON] = true,
  --[] = true,
 },
 [CU_SPELLS_INTERRUPTED] = {
  [KICK] = true,
  [HAMMER_OF_JUSTICE] = true,
  [WIND_SHEAR] = true,
  [SPELL_LOCK] = true,
  [COUNTERSPELL] = true,
  [REBUKE] = true,
  [SILENCE] = true,
  [MIND_FREEZE] = true,
  [MAIM_INTERRUPT] = true,
  [MIGHTY_BASH] = true,
  [SKULL_BASH_INTERRUPT] = true,
  [COUNTER_SHOT] = true,
  [SOLAR_BEAM_INTERRUPT] = true,
  [PUMMEL] = true,
  [SPEAR_HAND_STRIKE] = true,
  [CONSUME_MAGIC] = true,
  [ARCANE_TORRENT_MANA] = true,
  [ARCANE_TORRENT_ENERGY] = true,
  [ARCANE_TORRENT_RAGE] = true,
  [ARCANE_TORRENT_FOCUS] = true,
  [ARCANE_TORRENT_RUNIC_POWER] = true,
  [AVENGERS_SHIELD] = true,
  [QUAKING_PALM] = true,
 },
 [CU_SPELLS_INTERRUPT_USED_SUCCESS] = {
  [KICK] = true,
  [HAMMER_OF_JUSTICE] = true,
  [WIND_SHEAR] = true,
  [SPELL_LOCK] = true,
  [COUNTERSPELL] = true,
  [REBUKE] = true,
  [SILENCE] = true,
  [MIND_FREEZE] = true,
  [MAIM_INTERRUPT] = true,
  [MIGHTY_BASH] = true,
  [SKULL_BASH_INTERRUPT] = true,
  [SOLAR_BEAM_INTERRUPT] = true,
  [COUNTER_SHOT] = true,
  [PUMMEL] = true,
  [SPEAR_HAND_STRIKE] = true,
  [CONSUME_MAGIC] = true,
  [ARCANE_TORRENT_MANA] = true,
  [ARCANE_TORRENT_ENERGY] = true,
  [ARCANE_TORRENT_RAGE] = true,
  [ARCANE_TORRENT_FOCUS] = true,
  [ARCANE_TORRENT_RUNIC_POWER] = true,
  [AVENGERS_SHIELD] = true,
  [QUAKING_PALM] = true,
 },
 [CU_SPELLS_INTERRUPT_USED_FAILED] = {
  [KICK] = true,
  [HAMMER_OF_JUSTICE] = true,
  [WIND_SHEAR] = true,
  [SPELL_LOCK] = true,
  [COUNTERSPELL] = true,
  [REBUKE] = true,
  [SILENCE] = true,
  [MIND_FREEZE] = true,
  [MAIM_INTERRUPT] = true,
  [MIGHTY_BASH] = true,
  [SKULL_BASH_INTERRUPT] = true,
  [SOLAR_BEAM_INTERRUPT] = true,
  [COUNTER_SHOT] = true,
  [PUMMEL] = true,
  [SPEAR_HAND_STRIKE] = true,
  [CONSUME_MAGIC] = true,
  [ARCANE_TORRENT_MANA] = true,
  [ARCANE_TORRENT_ENERGY] = true,
  [ARCANE_TORRENT_RAGE] = true,
  [ARCANE_TORRENT_FOCUS] = true,
  [ARCANE_TORRENT_RUNIC_POWER] = true,
  [AVENGERS_SHIELD] = true,
  [QUAKING_PALM] = true,
 },
 [CU_SPELLS_TAUNTED] = {
  [RECKONING] = true,
  [GROWL] = true,
  [GROWL_PET] = true,
  [TAUNT] = true,
  [DARK_COMMAND] = true,
  [PROVOKE] = true,
 },
 [CU_SPELLS_TAUNT_MISSED] = {
  [RECKONING] = true,
  [GROWL] = true,
  [GROWL_PET] = true,
  [TAUNT] = true,
  [DARK_COMMAND] = true,
  [PROVOKE] = true,
 },
 [CU_SPELLS_COOLDOWN_USED] = {
  -- Death Knight
  [ICEBOUND_FORTITUDE] = true,
  [VAMPIRIC_BLOOD] = true,
  [DANCING_RUNE_WEAPON] = true,
  [ANTIMAGIC_SHELL] = true,
  [RAISE_ALLY] = true,
  --[] = true,
  -- Druid
  [REBIRTH] = true,
  [SURVIVAL_INSTINCTS] = true,
  [FRENZIED_REGENERATION] = true,
  [BARKSKIN] = true,
  --[] = true,
  -- Hunter
  [MISDIRECTION] = true,
  [FEIGN_DEATH] = true,
  [DETERRENCE] = true,
  --[] = true,
  -- Mage
  [ICE_BLOCK] = true,
  [INVISIBILITY] = true,
  [TIME_WARP] = true,
  --[] = true,
  -- Paladin
  [DIVINE_SHIELD] = true,
  [DIVINE_PROTECTION] = true,
  [BLESSING_OF_SACRIFICE] = true,
  [LAY_ON_HANDS] = true,
  [ARDENT_DEFENDER] = true,
  [AURA_MASTERY] = true,
  [BLESSING_OF_PROTECTION] = true,
  [GUARDIAN_ANCIENT_KINGS] = true,
  [SHIELD_OF_VENGEANCE] = true,
  --[] = true,
  -- Priest
  [PAIN_SUPPRESSION] = true,
  [GUARDIAN_SPIRIT] = true,
  [POWER_INFUSION] = true,
  [LEAP_OF_FAITH] = true,
  [POWER_WORD_BARRIER] = true,
  [SYMBOL_OF_HOPE] = true,
  [DIVINE_HYMN] = true,
  --[] = true,
  -- Rogue
  [TRICKOFTRADE] = true,
  [CLOAK_OF_SHADOWS] = true,
  --[] = true,
  -- Shaman
  [BLOOD_LUST] = true,
  [HEROISM] = true,
  [SPIRIT_LINK_TOTEM] = true,
  [SPIRIT_WALKER_GRACE] = true,
  [HEALING_TIDE_TOTEM] = true,
  [ASTRAL_SHIFT] = true,
  --[] = true,
  -- Warlock
  [SOUL_SHATTER] = true,
  --[] = true,
  -- Warrior
  [LAST_STAND] = true,
  [SHIELD_WALL] = true,
  -- Monk
  [FORTIFYING_BREW] = true,
  [ZEN_MEDITATION] = true,
  [DIFFUSE_MAGIC] = true,
  [DAMPEN_HARM] = true,
  [REVIVAL] = true,
  [LIFE_COCOON] = true,
  --[] = true,
 },
 [CU_SPELLS_RESURRECT] = {
  [REBIRTH] = true,
  [RAISE_ALLY] = true,
 },
};

CU_Template_Tank = {
 [CU_SPELLS_CREATIONS] = {
 },
 [CU_SPELLS_FEAST] = {
 },
 [CU_SPELLS_AURA_GAINED] = {
 },
 [CU_SPELLS_AURA_LOST] = {
 },
 [CU_SPELLS_INTERRUPTED] = {
 },
 [CU_SPELLS_INTERRUPT_USED_SUCCESS] = {
 },
 [CU_SPELLS_INTERRUPT_USED_FAILED] = {
 },
 [CU_SPELLS_TAUNTED] = {
 },
 [CU_SPELLS_TAUNT_MISSED] = {
 },
 [CU_SPELLS_COOLDOWN_USED] = {
 },
 [CU_SPELLS_RESURRECT] = {
 },
};

CU_Template_Heal = {
 [CU_SPELLS_CREATIONS] = {
 },
 [CU_SPELLS_FEAST] = {
 },
 [CU_SPELLS_AURA_GAINED] = {
 },
 [CU_SPELLS_AURA_LOST] = {
 },
 [CU_SPELLS_INTERRUPTED] = {
 },
 [CU_SPELLS_INTERRUPT_USED_SUCCESS] = {
 },
 [CU_SPELLS_INTERRUPT_USED_FAILED] = {
 },
 [CU_SPELLS_TAUNTED] = {
 },
 [CU_SPELLS_TAUNT_MISSED] = {
 },
 [CU_SPELLS_COOLDOWN_USED] = {
 },
 [CU_SPELLS_RESURRECT] = {
 },
};

CU_Template_Kicker = {
 [CU_SPELLS_CREATIONS] = {
 },
 [CU_SPELLS_FEAST] = {
 },
 [CU_SPELLS_AURA_GAINED] = {
 },
 [CU_SPELLS_AURA_LOST] = {
 },
 [CU_SPELLS_INTERRUPTED] = {
  [KICK] = true,
  [HAMMER_OF_JUSTICE] = true,
  [WIND_SHEAR] = true,
  [SPELL_LOCK] = true,
  [COUNTERSPELL] = true,
  [REBUKE] = true,
  [SILENCE] = true,
  [MIND_FREEZE] = true,
  [MAIM_INTERRUPT] = true,
  [MIGHTY_BASH] = true,
  [SKULL_BASH_INTERRUPT] = true,
  [SOLAR_BEAM_INTERRUPT] = true,
  [COUNTER_SHOT] = true,
  [PUMMEL] = true,
  [SPEAR_HAND_STRIKE] = true,
  [CONSUME_MAGIC] = true,
  [ARCANE_TORRENT_MANA] = true,
  [ARCANE_TORRENT_ENERGY] = true,
  [ARCANE_TORRENT_RAGE] = true,
  [ARCANE_TORRENT_FOCUS] = true,
  [ARCANE_TORRENT_RUNIC_POWER] = true,
  [AVENGERS_SHIELD] = true,
  [QUAKING_PALM] = true,
 },
 [CU_SPELLS_INTERRUPT_USED_SUCCESS] = {
  [KICK] = true,
  [HAMMER_OF_JUSTICE] = true,
  [WIND_SHEAR] = true,
  [SPELL_LOCK] = true,
  [COUNTERSPELL] = true,
  [REBUKE] = true,
  [SILENCE] = true,
  [MIND_FREEZE] = true,
  [MAIM_INTERRUPT] = true,
  [MIGHTY_BASH] = true,
  [SKULL_BASH_INTERRUPT] = true,
  [SOLAR_BEAM_INTERRUPT] = true,
  [COUNTER_SHOT] = true,
  [PUMMEL] = true,
  [SPEAR_HAND_STRIKE] = true,
  [CONSUME_MAGIC] = true,
  [ARCANE_TORRENT_MANA] = true,
  [ARCANE_TORRENT_ENERGY] = true,
  [ARCANE_TORRENT_RAGE] = true,
  [ARCANE_TORRENT_FOCUS] = true,
  [ARCANE_TORRENT_RUNIC_POWER] = true,
  [AVENGERS_SHIELD] = true,
  [QUAKING_PALM] = true,
 },
 [CU_SPELLS_INTERRUPT_USED_FAILED] = {
  [KICK] = true,
  [HAMMER_OF_JUSTICE] = true,
  [WIND_SHEAR] = true,
  [SPELL_LOCK] = true,
  [COUNTERSPELL] = true,
  [REBUKE] = true,
  [SILENCE] = true,
  [MIND_FREEZE] = true,
  [MAIM_INTERRUPT] = true,
  [MIGHTY_BASH] = true,
  [SKULL_BASH_INTERRUPT] = true,
  [SOLAR_BEAM_INTERRUPT] = true,
  [COUNTER_SHOT] = true,
  [PUMMEL] = true,
  [SPEAR_HAND_STRIKE] = true,
  [CONSUME_MAGIC] = true,
  [ARCANE_TORRENT_MANA] = true,
  [ARCANE_TORRENT_ENERGY] = true,
  [ARCANE_TORRENT_RAGE] = true,
  [ARCANE_TORRENT_FOCUS] = true,
  [ARCANE_TORRENT_RUNIC_POWER] = true,
  [AVENGERS_SHIELD] = true,
  [QUAKING_PALM] = true,
 },
 [CU_SPELLS_TAUNTED] = {
 },
 [CU_SPELLS_TAUNT_MISSED] = {
 },
 [CU_SPELLS_COOLDOWN_USED] = {
 },
 [CU_SPELLS_RESURRECT] = {
 },
};

