## Interface: 80000
## Title: GroupAnalyse
## Author: Kiki/Espêrance From European Cho'gall - Conseil des Ombres (Horde)
## Version: 8.2
## Notes: Analyse group status each time there is a change. Should be used by all addons, to prevent too much system calls
## DefaultState: Enabled
GroupAnalyse.xml
