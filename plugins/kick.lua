--[[
  Cooldown Used by Kiki/Espêrance - European Conseil des Ombres
   Kick plugin
  
  TODO:
   - affiche le CD de tous les joueurs qui ont un kick dispo (qu'on puisse trier par classe)
   - affiche le CD de tous les joueurs qui ont des sorts a CD (qu'on puisse filtrer, par classe, par nom, etc)
   - pouvoir mettre plusieurs sorts par classe
   - afficher une icone quand le joueur a reussi un interrupt
   - ajouter arcane torrent en detectant la classe et le type de power de chaque joueur


  BUGS:
   - 


]]

---------------------------------------------------------
-- Constantes
---------------------------------------------------------

local CU_KICK_SORT_NONE = 0;
local CU_KICK_SORT_AVAILABLE = 1;

---------------------------------------------------------
-- Variables
---------------------------------------------------------

local CU_Kick_MonitoredClass = {
  ["DEATHKNIGHT"] = {
    { spellID = 47528, cooldown = 15, role = "TANK" }, -- Mind Freeze (Blood)
    { spellID = 47528, cooldown = 15, role = "DAMAGER" }, -- Mind Freeze (Frost/Unholy)
  },
  ["DEMONHUNTER"] = {
    { spellID = 183752, cooldown = 15, role = "TANK" }, -- Disrupt (Vengeance)
    { spellID = 183752, cooldown = 15, role = "DAMAGER" }, -- Disrupt (Havoc)
  },
  ["DRUID"] = {
	--{ spellID = 78675, cooldown = 60, role = "DAMAGER", powertype = 8 }, -- Solar Beam (Balance)
    { spellID = 106839, cooldown = 15, role = "DAMAGER" }, -- Skull Bash (Feral)
    { spellID = 106839, cooldown = 15, role = "TANK" }, -- Skull Bash (Tank)
	--{ spellID = 97547, cooldown = 10, }, -- Solar Beam (??)
  },
  ["HUNTER"] = {
    { spellID = 147362, cooldown = 24, role = "DAMAGER" }, -- Counter Shot
  },
  ["MAGE"] = {
    { spellID = 2139, cooldown = 24, role = "DAMAGER" }, -- Counterspell
  },
  ["MONK"] = {
    { spellID = 116705, cooldown = 15, role = "TANK" }, -- Spear Hand Strike (Brewmaster)
    { spellID = 116705, cooldown = 15, role = "DAMAGER" }, -- Spear Hand Strike (Windwalker)
  },
  ["PALADIN"] = {
    { spellID = 96231, cooldown = 15, role = "TANK" }, -- Rebuke (Protection)
    { spellID = 96231, cooldown = 15, role = "DAMAGER" }, -- Rebuke (Retribution)
    --{ spellID = 853, cooldown = 60, role = "TANK" }, -- Hammer of Justice (Protection)
    --{ spellID = 853, cooldown = 60, role = "DAMAGER" }, -- Hammer of Justice (Retribution)
  },
  ["PRIEST"] = {
    { spellID = 15487, cooldown = 45, role = "DAMAGER" }, -- Silence
  },
  ["ROGUE"] = {
    { spellID = 1766, cooldown = 15, role = "DAMAGER" }, -- Kick
  },
  ["SHAMAN"] = {
    { spellID = 57994, cooldown = 12, role = "HEALER" }, -- Wind Shear
    { spellID = 57994, cooldown = 12, role = "DAMAGER" }, -- Wind Shear
  },
  ["WARLOCK"] = {
    --{ spellID = 132409, cooldown = 24, role = "DAMAGER" }, -- Spell Lock (Felhunter)
  },
  ["WARRIOR"] = {
    { spellID = 6552, cooldown = 15, role = "DAMAGER" }, -- Pummel (Arms/Fury)
    { spellID = 6552, cooldown = 15, role = "TANK" }, -- Pummel (Protection)
  },
};
local CU_Kick_Timers = {};
local CU_Kick_Ignored = {};

local CU_Kick_CurrentTime = 0;
local CU_Kick_LastTimeGUI = 0;
local CU_Kick_SortMode = CU_KICK_SORT_NONE;

local strformat = string.format;
local tinsert = table.insert;
local tremove = table.remove;
local tsort = table.sort;

---------------------------------------------------------
-- Status Frames
---------------------------------------------------------

local function CU_Kick_SetSparkPos(prefix,percent)
  local statusbar = _G[prefix.."StatusBar"];
  if(statusbar)
  then
    local bar_len = statusbar:GetWidth(); -- Add value if increased length
    _G[prefix.."StatusBarSpark"]:SetPoint("CENTER", _G[prefix.."StatusBar"],"LEFT",percent*bar_len/100,0);
  end
end

local function CU_Kick_EnableBars(enabled)
  local i = 1;
  for _,infos in ipairs(CU_Kick_Timers)
  do
    prefix = "CUK_Bar"..i;
    bar = _G[prefix];
    if(enabled)
    then
      bar:EnableMouse(true);
    else
      bar:EnableMouse(false);
    end
    i = i + 1;
  end
end

function CU_Kick_Anchor_OnClick(self,button)
  if(button == "LeftButton")
  then
    if(self:GetChecked())
    then
      -- Set unmovable
      self:GetParent().locked = true;
      -- Disable bars
      CU_Kick_EnableBars(false);
    else
      -- Set movable
      self:GetParent().locked = false;
      -- Enable bars
      CU_Kick_EnableBars(true);
    end
  elseif(button == "RightButton")
  then
    ToggleDropDownMenu(1,nil,CU_Kick_ConfigMenu,"cursor");
  end
end

function CU_Kick_SizeChanged(self,width,height)
  _G[self:GetName().."StatusBar"]:SetWidth(width-6);
  _G[self:GetName().."Border"]:SetWidth(width);
  _G[self:GetName().."Text"]:SetWidth(width-6);
  CU_Kick_SetSparkPos(self:GetName(),100);
end

local function CU_Kick_CreateFrame(name,anchor,id)
  local frame = CreateFrame("Frame",name,nil,"CU_Kick_BarTemplate");
  frame:ClearAllPoints();
  frame:SetPoint("TOPLEFT",anchor,"BOTTOMLEFT",0,-2);
  frame:SetWidth(80);
  frame:SetID(id);
  if(CU_Anchor.locked)
  then
    frame:EnableMouse(false);
  else
    frame:EnableMouse(true);
  end
  return frame;
end

local function CU_Kick_UpdateBars()
	local prefix;
	local percent;
	local remain;
	local divider;
	local bar;
	local tim = GetTime();
	local i = 1;

	-- Update bars
	for _,infos in ipairs(CU_Kick_Timers)
	do
		prefix = "CUK_Bar"..i;
		bar = _G[prefix];
		divider = infos.EndTime - infos.StartTime;
		percent = 100 - ((tim - infos.StartTime) / divider * 100);
		remain = infos.EndTime - tim;
		if(divider == 0 or percent < 0 or percent > 100)
		then
			percent = 0;
			remain = 0;
		end
		local r_col;
		local g_col;
		local pct = percent / 100;
		if(percent > 50)
		then
			r_col = 1.0;
			g_col = (1.0 - pct) * 2;
		else
			r_col = pct * 2;
			g_col = 1.0;
		end
		local statusbar = _G[prefix.."StatusBar"];
		local bartext = _G[prefix.."Text"];
		if(bar.name == nil) -- First set
		then
			bar.name = infos.name;
			-- Set Icon if available
			if(_G[prefix.."Icon"])
			then
				_G[prefix.."Icon"]:SetTexture(infos.texture);
			end
		end
		-- Set time text, if available
		local timetext = _G[prefix.."TimeText"];
		if(timetext)
		then
			if(remain == 0)
			then
				timetext:SetText("Rdy");
			elseif(remain >= 10)
			then
				timetext:SetText(strformat("%d s",remain));
			else
				timetext:SetText(strformat("%.1f s",remain));
			end
			timetext:SetTextColor(r_col,g_col,0.1);
			if(bartext)
			then
				_G[prefix.."Text"]:SetText(infos.name);
			end
		end
		if(statusbar)
		then
			statusbar:SetValue(percent);
			statusbar:SetStatusBarColor(r_col,g_col,0.1,1);
		end
		CU_Kick_SetSparkPos(prefix,percent);
		i = i + 1;
	end
end

local function CU_Kick_ResetBarName()
	for i in ipairs(CU_Kick_Timers)
	do
		local frame = _G["CUK_Bar"..i];
		frame.name = nil;
	end
end

local function CU_Kick_HideRemainingBars()
	local i = #CU_Kick_Timers + 1;
	local frame = _G["CUK_Bar"..i];
	while(frame)
	do
		frame.name = nil;
		frame:Hide();
		i = i + 1;
		frame = _G["CUK_Bar"..i];
	end
end


---------------------------------------------------------
-- Timer functions
---------------------------------------------------------

local function CU_Kick_CreateKickerCooldown(name,fullname,spellID,cooldown,r,g,b)
	local old_count = #CU_Kick_Timers;
	local new_count = old_count + 1;
	local fname = "CUK_Bar"..new_count;
	local frame = _G[fname];
	if(frame == nil) -- Must create the frame
	then
		local anchor;
		if(new_count == 1) -- First frame
		then
			anchor = "CU_Anchor";
		else
			anchor = "CUK_Bar"..old_count;
		end
		frame = CU_Kick_CreateFrame(fname,anchor,new_count);
	end
	local infos = GA_GetTable();
	infos.name = name;
	infos.fullname = fullname;
	infos.r = r or 0.2;
	infos.g = g or 0.9;
	infos.b = b or 0.1;
	infos.spellID = spellID;
	infos.texture = select(3,GetSpellInfo(spellID));
	infos.StartTime = GetTime();
	infos.EndTime = infos.StartTime;
	infos.Cooldown = cooldown;
	tinsert(CU_Kick_Timers,infos);
	frame:Show();
	CU_Kick_UpdateBars();
end

local function CU_Kick_CreateKicker(infos)
	local class_kicks = CU_Kick_MonitoredClass[infos.class];
	if(class_kicks)
	then
		-- Check not already added
		for _,inf in ipairs(CU_Kick_Timers)
		do
			if(infos.fullname == inf.fullname)
			then
				return
			end
		end
		
		-- Add all kick spells for this player
		for _,kick in ipairs(class_kicks)
		do
			if(--[[kick.role == nil or]] kick.role == infos.role)
			then
				CU_Kick_CreateKickerCooldown(infos.name,infos.fullname,kick.spellID,kick.cooldown);
			end
		end
	end
end

local function CU_Kick_RemoveKicker(fullname) -- Full name
	local found = false;
	repeat
		found = false;
		for i,infos in ipairs(CU_Kick_Timers)
		do
			if(infos.fullname == fullname)
			then
				local inf = infos;
				tremove(CU_Kick_Timers,i);
				GA_ReleaseTable(inf);
				found = true;
				break;
			end
		end
	until(found == false)
	CU_Kick_ResetBarName();
	CU_Kick_HideRemainingBars();
	CU_Kick_UpdateBars();
end

local function CU_Kick_Sort_Available(a,b)
	return a.EndTime < b.EndTime;
end

local function CU_Kick_CheckSorting()
	if(CU_Kick_SortMode == CU_KICK_SORT_AVAILABLE)
	then
		tsort(CU_Kick_Timers,CU_Kick_Sort_Available);
	end
end

local function CU_Kick_StartCooldown(fullname,spellID)
	local must_refresh = false;
	for i,infos in ipairs(CU_Kick_Timers)
	do
		if(infos.fullname == fullname and infos.spellID == spellID)
		then
			infos.fullname = fullname;
			infos.StartTime = GetTime();
			infos.EndTime = infos.StartTime + infos.Cooldown;
			must_refresh = true;
		end
	end
	if(must_refresh)
	then
		CU_Kick_CheckSorting();
		CU_Kick_UpdateBars(); -- Update bars now
	end
end

function CU_Kick_SetSorting(mode)
	if(CU_Kick_SortMode ~= mode)
	then
		CU_Kick_SortMode = mode;
		CU_Kick_CheckSorting();
		CU_Kick_UpdateBars();
	end
end

---------------------------------------------------------
-- Callbacks
---------------------------------------------------------

local function CU_Kick_SpellCastSuccess(sourceName,destName,spellID,spellName)
	CU_Kick_StartCooldown(sourceName,spellID);
end

local function CU_Kick_GACB(event,param,subevent)
	if(event == GroupAnalyse.EVENT_MEMBER_JOINED)
	then
		if(GroupAnalyse.CurrentGroupMode ~= GroupAnalyse.MODE_RAID)
		then
			local infos = GroupAnalyse.Members[param];
			if(not infos.ispet)
			then
				CU_Kick_CreateKicker(infos);
			end
		end
	elseif(event == GroupAnalyse.EVENT_MEMBER_LEFT)
	then
		CU_Kick_RemoveKicker(param);
	elseif(event == GroupAnalyse.EVENT_ROLE_CHANGED)
	then
		CU_Kick_RemoveKicker(param.fullname); -- Remove previous spells for this player
		CU_Kick_CreateKicker(param); -- Recreate kicker
	elseif(event == GroupAnalyse.EVENT_GROUP_MODE_CHANGED)
	then
		if(param ~= GroupAnalyse.MODE_RAID)
		then
			CU_KickMainFrame:Show();
			CU_Anchor:Show();
			CU_Kick_CreateKicker(GroupAnalyse.Myself); -- Recreate myself
		else
			while(CU_Kick_Timers[1])
			do
				CU_Kick_RemoveKicker(CU_Kick_Timers[1].fullname);
			end
			CU_KickMainFrame:Hide();
			CU_Anchor:Hide();
		end
	end
end

---------------------------------------------------------
-- Config Menu
---------------------------------------------------------

function CU_Kick_ConfigMenu_OnLoad(self)
	HideDropDownMenu(1);
	self.initialize = CU_Kick_ConfigMenu_OnInitialize;
	self.displayMode = "MENU";
	self.name = "Titre";
end

function CU_Kick_ConfigMenu_OnInitialize(frame,level)
	info = UIDropDownMenu_CreateInfo();
	info.text =  "Kick Module Config";
	info.isTitle = true;
	info.notCheckable = 1;
	UIDropDownMenu_AddButton(info);

	-- Sub menu, ignored kickers

	-- Sub menu, sort type

	info = UIDropDownMenu_CreateInfo();
	info.text = CANCEL;
	info.notCheckable = 1;
	info.func = CU_KickM_Cancel_OnClick;
	UIDropDownMenu_AddButton(info);
end

function CU_KickM_Cancel_OnClick()
	HideDropDownMenu(1);
end

---------------------------------------------------------
-- Kicker Menu
---------------------------------------------------------

local function CU_KickM_IgnoreKicker(self,kicker,id)
	tinsert(CU_Kick_Ignored,kicker);
	tremove(CU_Kick_Timers,id);
	-- Update bars
	CU_Kick_ResetBarName();
	CU_Kick_HideRemainingBars();
	CU_Kick_UpdateBars();
end

local function CU_KickM_MoveUp(self,kicker,id)
	CU_Kick_Timers[id] = CU_Kick_Timers[id-1];
	CU_Kick_Timers[id-1] = kicker;
	-- Update bars
	CU_Kick_ResetBarName();
	CU_Kick_UpdateBars();
end

local function CU_KickM_MoveDown(self,kicker,id)
	CU_Kick_Timers[id] = CU_Kick_Timers[id+1];
	CU_Kick_Timers[id+1] = kicker;
	-- Update bars
	CU_Kick_ResetBarName();
	CU_Kick_UpdateBars();
end

function CU_Kick_KickerMenu_OnLoad(self)
	HideDropDownMenu(1);
	self.initialize = CU_Kick_KickerMenu_OnInitialize;
	self.displayMode = "MENU";
	self.name = "Titre";
end

function CU_Kick_KickerMenu_OnInitialize(frame,level)
	local kicker = CU_Kick_Timers[UIDROPDOWNMENU_MENU_VALUE];

	info = UIDropDownMenu_CreateInfo();
	info.text =  kicker.name;
	info.isTitle = true;
	info.notCheckable = 1;
	UIDropDownMenu_AddButton(info);

	info = UIDropDownMenu_CreateInfo();
	info.text = "Ignore kicker";
	info.arg1 = kicker;
	info.arg2 = UIDROPDOWNMENU_MENU_VALUE;
	info.func = CU_KickM_IgnoreKicker;
	info.notCheckable = 1;
	UIDropDownMenu_AddButton(info);

	info = UIDropDownMenu_CreateInfo();
	info.text =  "Move up";
	info.arg1 = kicker;
	info.arg2 = UIDROPDOWNMENU_MENU_VALUE;
	info.func = CU_KickM_MoveUp;
	info.disabled = UIDROPDOWNMENU_MENU_VALUE <= 1;
	info.notCheckable = 1;
	UIDropDownMenu_AddButton(info);

	info = UIDropDownMenu_CreateInfo();
	info.text =  "Move down";
	info.arg1 = kicker;
	info.arg2 = UIDROPDOWNMENU_MENU_VALUE;
	info.func = CU_KickM_MoveDown;
	info.disabled = UIDROPDOWNMENU_MENU_VALUE >= #CU_Kick_Timers;
	info.notCheckable = 1;
	UIDropDownMenu_AddButton(info);

	-- Sub menu, ignored kickers

	-- Other options

	info = UIDropDownMenu_CreateInfo();
	info.text = CANCEL;
	info.notCheckable = 1;
	info.func = CU_KickM_Cancel_OnClick;
	UIDropDownMenu_AddButton(info);
end

---------------------------------------------------------
-- Main Frame
---------------------------------------------------------

local CU_Kick_Callbacks = {
	[CU_SPELL_CAST_SUCCESS] = CU_Kick_SpellCastSuccess,
};

function CU_Kick_OnUpdate(self,elapsed)
	CU_Kick_CurrentTime = CU_Kick_CurrentTime + elapsed;

	-- Update GUI every 0.10 sec
	if(CU_Kick_CurrentTime > (CU_Kick_LastTimeGUI+0.10))
	then
		CU_Kick_UpdateBars();
		CU_Kick_LastTimeGUI = CU_Kick_CurrentTime;
	end
end

local function CU_Kick_EnableModule()
	if(CU_Config.Modules["Kick"] == nil)
	then
		CU_Config.Modules["Kick"] = {};
	end
	GroupAnalyse.RegisterForEvents(CU_Kick_GACB);
	CU_KickMainFrame:Show();
	CU_Anchor:Show();
	-- Set unmovable
	CU_AnchorButton:SetChecked(1);
	CU_Anchor.locked = true;
	-- Disable bars
	CU_Kick_EnableBars(false);
	CU_Anchor.locked = true;
end

local function CU_Kick_DisableModule()
	GroupAnalyse.UnRegisterForEvents(CU_Kick_GACB);
	while(CU_Kick_Timers[1])
	do
		CU_Kick_RemoveKicker(CU_Kick_Timers[1].fullname);
	end
	CU_KickMainFrame:Hide();
	CU_Anchor:Hide();
end

CU_RegisterModule("Kick",CU_Kick_EnableModule,CU_Kick_DisableModule,CU_Kick_Callbacks);

--CUK_Bar1:SetMovable(1);
